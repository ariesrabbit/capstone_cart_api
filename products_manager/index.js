var dssv = [];
var BASE_URL = "https://630092419a1035c7f8f46aac.mockapi.io";
var batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
var tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};

var renderDsspService = function () {
  batLoading();
  axios({
    url: `${BASE_URL}/qlsp`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      dssv = res.data;
      renderTable(dssv);
    })
    .catch(function (err) {
      tatLoading();

      console.log(err);
    });
};
renderDsspService();
function xoaSP(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/qlsp/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      renderDsspService();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();

      console.log(err);
    });
}

function themSP() {
  var dataForm = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/qlsp`,
    method: "POST",
    data: dataForm,
  })
    .then(function (res) {
      tatLoading();
      renderDsspService();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
function suaSP(id) {
  console.log("id: ", id);
  batLoading();
  axios({
    url: `${BASE_URL}/qlsp/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      tatLoading();
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
function capNhatSP() {
  var dataForm = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/qlsp/${dataForm.ma}`,
    method: "PUT",
    data: dataForm,
  })
    .then(function (res) {
      console.log(res);
      tatLoading();
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
