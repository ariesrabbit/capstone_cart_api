var renderTable = function (list) {
  var contentHTML = "";
  list.forEach(function (item) {
    var trContent = `
   <tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.type}</td>
    <td>${item.screen}</td>
   
    <td>${item.frontCamera}</td>
    <td>${item.backCamera}</td>
    <td>${item.desc}</td>
    <td>${item.price}</td>
    <td>
    <img src=${item.img} style="width:80px" alt="" />
 </td>
    <td>
    <button
    onclick="xoaSP('${item.id}')"
    class="btn btn-danger m-1">Xoá</button>
    <button
    onclick="suaSP('${item.id}')"
    class="btn btn-success m-1">Sửa</button>
    </td>
  </tr>
    `;
    contentHTML += trContent;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};
function layThongTinTuForm() {
  let name = document.getElementById("txtTenSP").value;
  let type = document.getElementById("txtType").value;
  let screen = document.getElementById("txtScreen").value;
  let frontCamera = document.getElementById("txtFrontCam").value;
  let backCamera = document.getElementById("txtBackCam").value;
  let img = document.getElementById("txtImg").value;
  let price = document.getElementById("txtPrice").value;
  let desc = document.getElementById("txtDesc").value;

  return {
    name: name,
    type: type,
    screen: screen,
    frontCamera: frontCamera,
    backCamera: backCamera,
    img: img,
    price: price,
    desc: desc,
  };
}
function showThongTinLenForm(data) {
  document.getElementById("txtTenSP").value = data.name;
  document.getElementById("txtType").value = data.type;
  document.getElementById("txtScreen").value = data.screen;
  document.getElementById("txtFrontCam").value = data.frontCamera;
  document.getElementById("txtBackCam").value = data.backCamera;
  document.getElementById("txtImg").value = data.img;
  document.getElementById("txtPrice").value = data.price;
  document.getElementById("txtDesc").value = data.desc;
}
